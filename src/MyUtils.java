
import java.util.Calendar;

/**
 *
 * @author Lluis
 * Classe de funcions pròpies utils.
 */
public class MyUtils 
{
    /**
     * 
     * @param cadena Strint cadena que es vol invertir
     * @return cadena invertida (null per cadenes nulls).
     */
    public static String inverteix (String cadena)
    {
        String resultat=null;
        if (cadena!=null){
            resultat = new StringBuilder(cadena).reverse().toString();
        }
        return resultat;
    }
    /**
     * 
     * @param day int dia del naixement
     * @param month int  mes del naixement
     * @param year int any del naixement
     * @return edat de la persona, per edat>150 retorna -1, per dates impossibles retorna -2
     * 
     */
    public static int edat (int day, int month, int year)
    {
        int diaActual = Calendar.DAY_OF_MONTH;
        int mesActual = Calendar.MONTH;
        int añoActual = Calendar.YEAR;

        int resultat = 0;

        // Condicio dies
        if(day <= 0 || day > 31) 
        {
            resultat = -2;
        }
        // Condicio mesos
        if(month <= 0 || month > 12) 
        {
            resultat = -2;
        }
        // Condicio any
         if(year <= 0 || year > añoActual) 
        {
            resultat = -2;
        }

        int resultatAny = añoActual - year;
        if(month < mesActual) resultat = resultatAny - 1;
        else if(month == mesActual)
        {
            if(day < diaActual) resultat = resultatAny - 1;
            // Si es el mateix dia donem per suposat que ja ha ocmplert anys
            else resultat = resultatAny;
        }
        else resultat = resultatAny;

        if(resultatAny >= 150) resultat = -1;

        return resultat;
    }

    /**
     * 
     * @param numero número del que es calcula el factorial
     * @return retorna el factorial d'un número. Si el nombre es negatiu retorna -1.
     */
    public static int factorial (int numero) 
    {   
        int resultat = 1;

        if (numero==0)
            resultat = 1;
        else if(numero < 0)
            resultat = -1;
        else
        {
            //int numeroWhile = numero;
            //while(numeroWhile > 1)
            for(int i=numero; i>1; i--)
            {
                resultat = resultat*i;
               // numeroWhile--;
            }
        }
        return resultat;
    }
}