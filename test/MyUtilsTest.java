/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author alumneDAM
 */
public class MyUtilsTest {
    
    public MyUtilsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of inverteix method, of class MyUtils.
     */
    //@Test
    //@Ignore
    //public void testInverteix() {
       // System.out.println("inverteix");
       // String cadena = "Hola";
       // String expResult = "aloH";
       // String result = MyUtils.inverteix(cadena);
       // assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
   // }
    @Test
    public void inverteixFinal(){//nous tests
       assertEquals("aloH", MyUtils.inverteix("Hola"));
       assertEquals("uedA", MyUtils.inverteix("Adeu"));
    }
    
    @Test
    public void inverteixNull(){//nous tests
       assertEquals("llun", MyUtils.inverteix("null"));
    }
    
     @Test
    public void inverteixNumero(){//nous tests
       assertEquals("Vinticinc", MyUtils.inverteix("cnicitniV"));
    }

    /**
     * Test of edat method, of class MyUtils.
     */
   
    //public void testEdat() {
       // System.out.println("edat");
       // int day = 21;
        //int month = 12;
        //int year = 1995;
        //int expResult = 21;
        //int result = MyUtils.edat(day, month, year);
        //assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
   // }
    @Test
    public void test150(){//nou test
        assertEquals(-1199, MyUtils.edat(22, 3, 1200));
    }

    /**
     * Test of factorial method, of class MyUtils.
     */
    @Test
   
    public void testFactorial() {
        System.out.println("factorial");
        int numero = 5;
        int expResult = 120;
        int result = MyUtils.factorial(numero);
        assertEquals(expResult, result);//Test Factorial
        assertTrue("This will succed", result > 100);//Test AssertTrue
       // assertTrue("This will fail", result > 121);
       // assertFalse("This will fail", result > 100);
        assertFalse("This will succed", result > 121);//Test AssertFalse
        
    }
    
}
